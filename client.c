#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
int sockfd;
void* start_run(void* arg)
{
	for(;;)
	{
		char buf[1024]={};
		recv(sockfd,buf,sizeof(buf),0);
		printf("read:%s\n",buf);
	}
}
int main()
{
    printf("创建socket......\n");
    sockfd=socket(AF_INET,SOCK_STREAM,0);
    if(0>sockfd)
    {
        perror("socket");
    }
    struct sockaddr_in addr = {};
	addr.sin_family = AF_INET;
	addr.sin_port = htons(6432);
	addr.sin_addr.s_addr = inet_addr("10.0.2.15");
	int len = sizeof(addr);

	printf("连接服务器...\n");
	if(connect(sockfd,(struct sockaddr*)&addr,len))
	{
		perror("connect");
		return -1;
	}
	for(;;)
	{
			pthread_t pid;
			pthread_create(&pid,NULL,start_run,NULL);
			char buf[1024] = {};
			printf(">");
			gets(buf);
			send(sockfd,buf,strlen(buf)+1,0);
			if(0 == strcmp("quit",buf))
			{
				printf("ͨ通信结束!\n");
				break;
			}
	}
	close(sockfd);
}

