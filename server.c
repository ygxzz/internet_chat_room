#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
int k=0;
struct in_addr sin_addr[100];
int clifd[100];
int sockfd,flag=0;
void* start_run(void* arg)
{
	int clifd1=*(int*)arg;
    for(;;)
    {
		char buf[1024] = {};
		char buf1[1024] = {};
		// 回声服务器
		recv(clifd1,buf,sizeof(buf),0);
		for(int i=0;i<k;i++)
		{
			if(clifd[i]==clifd1)
			{
				printf("read:%s from:ip%s\n",buf,inet_ntoa(sin_addr[i]));
			}	
		}
		if(0 == strcmp("quit",buf))
		{
			close(sockfd);
			return 0;
		}
		for(int i=0;i<k;i++)
		{
			send(clifd[i],buf,strlen(buf)+1,0);
		}
	}
}
int main()
{
	printf("服务器创建socket...\n");
	sockfd = socket(AF_INET,SOCK_STREAM,0);
	if(0 > sockfd)
	{
		perror("socket");
		return -1;
	}

	printf("准备地址...\n");
	struct sockaddr_in addr = {};
	addr.sin_family = AF_INET;
	addr.sin_port = htons(6432);
	addr.sin_addr.s_addr = inet_addr("10.0.2.15");
	int len = sizeof(addr);

	printf("绑定socket与地址...\n");
	if(bind(sockfd,(struct sockaddr*)&addr,len))
	{
		perror("bind");
		return -1;
	}

	printf("设置监听...\n");
	if(listen(sockfd,5))
	{
		perror("listen");
		return -1;
	}

	printf("等待客户端连接...\n");
	for(;;)
	{
		struct sockaddr_in addrcli = {};
		clifd[k] = accept(sockfd,(struct sockaddr*)&addrcli,&len);
		sin_addr[k]=addrcli.sin_addr;
		k++;
		if(0 > clifd[k-1])
		{
			perror("accept");
			break;
		}
		pthread_t pid;
		pthread_create(&pid,NULL,start_run,&clifd[k-1]);
	}
	close(sockfd);
}

